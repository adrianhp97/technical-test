<?php

namespace App;

use Validator;
use Illuminate\Database\Eloquent\Model;

class Kandidat extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'kandidat';
    public $timestamps = true;
    public $fillable = [
        'nama_lengkap',
        'jenis_kelamin',
        'alamat',
        'no_hp',
        'email',
        'lowongan_id',
        'status',
        'file'
    ];
    private $rules = array(
        'nama_lengkap' => 'required|string',
        'jenis_kelamin' => 'required',
        'alamat' => 'required',
        'no_hp' => 'required|numeric',
        'email' => 'required|email',
        'lowongan_id' => 'required',
        'status' => 'required',
        'file' => 'required',
    );
    private $errors;

    public function lowongan()
    {
    	return $this->belongsTo('App\Lowongan');
    }

    public function validate($data)
    {
        $validator = Validator::make($data, $this->rules);
        if ($validator->fails())
        {
            $this->errors = $validator->errors()->toArray();
            return false;
        }
        return true;
    }

    public function errors()
    {
        return $this->errors;
    }
}
