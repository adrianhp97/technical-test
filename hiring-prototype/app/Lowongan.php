<?php

namespace App;

use Validator;
use Illuminate\Database\Eloquent\Model;

class Lowongan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'lowongan';
    public $timestamps = true;
    public $fillable = [
        'nama_lowongan',
        'job_desc',
        'skill',
        'status'
    ];
    private $rules = array(
        'nama_lowongan' => 'required',
        'job_desc' => 'required',
        'skill' => 'required',
        'status' => 'required'
    );
    private $errors;

    public function kandidat()
    {
    	return $this->hasMany('App\Kandidat');
    }

    public function validate($data)
    {
        $validator = Validator::make($data, $this->rules);
        if ($validator->fails())
        {
            $this->errors = $validator->errors()->toArray();
            return false;
        }
        return true;
    }

    public function errors()
    {
        return $this->errors;
    }
}
