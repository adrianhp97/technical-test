<?php

namespace App\Http\Controllers;

use Session;
use App\Lowongan;
use Illuminate\Http\Request;

class LowonganController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.lowongan.index', ['lowongans' => Lowongan::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.lowongan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $lowongan = new Lowongan();
        if ($lowongan->validate($data))
        {
            Lowongan::create($data);
            Session::flash('flash_message', 'Lowongan successfully added!');
        }
        else
        {
            $errors = $lowongan->errors();
            return redirect()
                ->back()
                ->withErrors($errors)
                ->withInput();
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lowongan  $lowongan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.lowongan.show', ['lowongan' => Lowongan::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lowongan  $lowongan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.lowongan.edit', ['lowongan' => Lowongan::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lowongan  $lowongan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $lowongan = Lowongan::findOrFail($id);
        if ($lowongan->validate($data))
        {
            $lowongan->fill($data)->save();
            Session::flash('flash_message', 'Task successfully added!');
        }
        else
        {
            $errors = $lowongan->errors();
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lowongan  $lowongan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lowongan = Lowongan::findOrFail($id);

        $lowongan->delete();

        Session::flash('flash_message', 'Lowongan successfully deleted!');

        return redirect()->route('lowongan.index');
    }
}
