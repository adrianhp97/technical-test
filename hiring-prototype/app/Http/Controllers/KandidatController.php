<?php

namespace App\Http\Controllers;

use Session;
use Storage;
use App\Kandidat;
use App\Lowongan;
use Illuminate\Http\Request;

class KandidatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kandidat = Kandidat::all();
        if ($request->has('nama_lengkap')) {
            if (!empty($request->input('nama_lengkap'))) {
                $kandidat = Kandidat::where('nama_lengkap', 'like', '%' . $request->input('nama_lengkap') . '%')->get();
            }
        }

        if ($request->has('lowongan_id')) {
            if (!empty($request->input('lowongan_id'))) {
                $kandidat = $kandidat->where('lowongan_id', $request->input('lowongan_id'));
            }
        }
        
        if ($request->has('created_at')) {
            if (!empty($request->input('created_at'))) {
                $kandidat = $kandidat->where('created_at', $request->input('created_at'));
            }
        }

        $collection = collect(Lowongan::all(['id', 'nama_lowongan']));
        $lowongan = $collection->mapWithKeys(function ($item) {
            return [$item['id'] => $item['nama_lowongan']];
        });
        return view('admin.kandidat.index', ['kandidats' => $kandidat, 'lowongan' => $lowongan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $collection = collect(Lowongan::all(['id', 'nama_lowongan']));
        $lowongan = $collection->mapWithKeys(function ($item) {
            return [$item['id'] => $item['nama_lowongan']];
        });
        return view('admin.kandidat.create', ['lowongan' => $lowongan]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $kandidat = new Kandidat();
        if ($kandidat->validate($data))
        {
            $path = $request->file('file')->storeAs(
                'file', $request->file('file')->getClientOriginalName()
            );

            $data['file'] = $path;
            Kandidat::create($data);
            Session::flash('flash_message', 'kandidat successfully added!');
        }
        else
        {
            $errors = $kandidat->errors();
            return redirect()
                ->back()
                ->withErrors($errors)
                ->withInput();
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kandidat  $kandidat
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.kandidat.show', ['kandidat' => Kandidat::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kandidat  $kandidat
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $collection = collect(Lowongan::all(['id', 'nama_lowongan']));
        $lowongan = $collection->mapWithKeys(function ($item) {
            return [$item['id'] => $item['nama_lowongan']];
        });
        return view('admin.kandidat.edit', ['kandidat' => Kandidat::findOrFail($id), 'lowongan' => $lowongan]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kandidat  $kandidat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $kandidat = Kandidat::findOrFail($id);
        if ($kandidat->validate($data))
        {
            $kandidat->fill($data)->save();
            Session::flash('flash_message', 'Task successfully added!');
        }
        else
        {
            $errors = $kandidat->errors();
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kandidat  $kandidat
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kandidat = Kandidat::findOrFail($id);

        $kandidat->delete();

        Session::flash('flash_message', 'kandidat successfully deleted!');

        return redirect()->route('kandidat.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function daftar()
    {
        $collection = collect(Lowongan::all(['id', 'nama_lowongan']));
        $lowongan = $collection->mapWithKeys(function ($item) {
            return [$item['id'] => $item['nama_lowongan']];
        });
        return view('user.create', ['lowongan' => $lowongan]);
    }

    public function download($path) 
    {
        return Storage::disk('local')->get($path);
    }
}
