<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin123',
            'username' => 'admin123',
            'email' => 'admin123@gmail.com',
            'password' => bcrypt('admin123'),
            'type' => 'admin',
        ]);

        DB::table('users')->insert([
            'name' => 'guest123',
            'username' => 'guest123',
            'email' => 'guest123@gmail.com',
            'password' => bcrypt('guest123'),
            'type' => 'guest',
        ]);
    }
}
