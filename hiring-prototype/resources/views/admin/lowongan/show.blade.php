@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">
                    Lowongan
                    <a href="{{ route('lowongan.index') }}" class="btn btn-primary pull-right">Back</a> 
                </div>

                <div class="card-body">
                    <h1>{{ $lowongan->nama_lowongan }}</h1>
                    &nbsp
                    <div>
                        <h4>Job Description:</h4>
                        <p class="lead">{{ $lowongan->job_desc }}</p>
                    </div>
                    <hr>
                    <div>
                        <h4>Skill:</h4>
                        <p class="lead">{{ $lowongan->skill }}</p>
                    </div>
                    <hr>
                    <p class="lead">{{ $lowongan->status }}</p>
                    <hr>

                    <a href="{{ route('lowongan.edit', $lowongan->id) }}" class="btn btn-primary">Edit Lowongan</a>

                    <div class="col-md-6 text-right pull-right">
                        {!! Form::open([
                            'method' => 'DELETE',
                            'route' => ['lowongan.destroy', $lowongan->id]
                        ]) !!}
                            {!! Form::submit('Delete this lowongan?', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection