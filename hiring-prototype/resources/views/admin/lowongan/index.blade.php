@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">
                    Lowongan
                    <a href="{{ route('lowongan.create') }}" class="btn btn-primary pull-right">Create</a> 
                </div>

                <div class="card-body">
                    @if(count($lowongans) > 0)
                    <table>
                        <thead>
                            <tr>
                                <th>Nama Lowongan</th>
                                <th>Job Description</th>
                                <th>Skill</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($lowongans as $lowongan)
                            <tr>
                                <td> {{ $lowongan->nama_lowongan }} </td>
                                <td> {{ $lowongan->job_desc }} </td>
                                <td> {{ $lowongan->skill }} </td>
                                <td> {{ $lowongan->status }} </td>
                                <td> 
                                    <a href="{{ route('lowongan.show', $lowongan->id) }}" class="btn btn-info">View</a>
                                    <a href="{{ route('lowongan.edit', $lowongan->id) }}" class="btn btn-primary">Edit</a> 
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <p> No lowongan found..</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection