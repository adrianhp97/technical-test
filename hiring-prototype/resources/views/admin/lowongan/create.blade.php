@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">
                    Lowongan
                    <a href="{{ route('lowongan.index') }}" class="btn btn-primary pull-right">Back</a> 
                </div>

                <div class="card-body">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif

                    @if(Session::has('flash_message'))
                        <div class="alert alert-success">
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    
                    {!! Form::open([
                        'route' => 'lowongan.store'
                    ]) !!}

                    <div class="form-group">
                        {!! Form::label('nama_lowongan', 'Nama Lowongan:', ['class' => 'control-label']) !!}
                        {!! Form::text('nama_lowongan', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('job_desc', 'Job Description:', ['class' => 'control-label']) !!}
                        {!! Form::textarea('job_desc', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('skill', 'Skill:', ['class' => 'control-label']) !!}
                        {!! Form::textarea('skill', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('status', 'Status:', ['class' => 'control-label']) !!}
                        {!! Form::select('status', array('publish' => 'Publish', 'unpublish' => 'Unpublish'), 'publish', ['class' => 'form-control']) !!}
                    </div>


                    {!! Form::submit('Create New Lowongan', ['class' => 'btn btn-primary']) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection