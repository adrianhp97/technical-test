@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">
                    Kandidat
                    <a href="{{ route('kandidat.create') }}" class="btn btn-primary pull-right">Create</a> 
                </div>

                <div class="card-body">
                {!! Form::open([
                    'method' => 'GET',
                    'route' => ['kandidat.index']
                ]) !!}
                <div class="row">

                    <div class="form-group col-md-3">
                        {!! Form::label('nama_lengkap', 'Search:', ['class' => 'control-label']) !!}
                        {!! Form::text('nama_lengkap', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group col-md-3">
                        {!! Form::label('lowongan_id', 'Lowongan:', ['class' => 'control-label']) !!}
                        {!! Form::select('lowongan_id', array_merge(['' => 'Please Select'], $lowongan->toArray()), null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group col-md-3">
                        {!! Form::label('created_at', 'Date:', ['class' => 'control-label']) !!}
                        {!! Form::date('created_at', null, ['class' => 'form-control']) !!}
                    </div>
                    
                    <div class="col-md-3">
                        {!! Form::submit('Search', ['class' => 'btn btn-primary pull-right']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
                @if(count($kandidats) > 0)
                    <table>
                        <thead>
                            <tr>
                                <th>Nama Lengkap</th>
                                <th>Jenis Kelamin</th>
                                <th>Alamat</th>
                                <th>No Handphone</th>
                                <th>Email</th>
                                <th>Lowongan</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($kandidats as $kandidat)
                            <tr>
                                <td> {{ $kandidat->nama_lengkap }} </td>
                                <td> {{ $kandidat->jenis_kelamin }} </td>
                                <td> {{ $kandidat->alamat }} </td>
                                <td> {{ $kandidat->no_hp }} </td>
                                <td> {{ $kandidat->email }} </td>
                                <td> {{ $kandidat->lowongan->nama_lowongan }} </td>
                                <td> {{ $kandidat->status }} </td>
                                <td> 
                                    <a href="{{ route('kandidat.show', $kandidat->id) }}" class="btn btn-info">View</a>
                                    <a href="{{ route('kandidat.edit', $kandidat->id) }}" class="btn btn-primary">Edit</a> 
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <p> No kandidat found..</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection