@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">
                    Kandidat
                    <a href="{{ route('kandidat.index') }}" class="btn btn-primary pull-right">Back</a> 
                </div>

                <div class="card-body">
                    <h1>{{ $kandidat->nama_lengkap }}</h1>
                    <p class="lead"><b>Jenis Kelamin</b>: {{ $kandidat->jenis_kelamin }}</p>
                    <p class="lead"><b>Alamat</b>: {{ $kandidat->alamat }}</p>
                    <p class="lead"><b>No Handphone</b>: {{ $kandidat->no_hp }}</p>
                    <p class="lead"><b>Email</b>: {{ $kandidat->email }}</p>
                    <p class="lead"><b>Lowongan</b>: {{ $kandidat->lowongan->nama_lowongan }}</p>
                    <p class="lead"><b>Status</b>: {{ $kandidat->status }}</p>
                    <p class="lead"><b>File</b>: <a href="{{route('download-cv', $kandidat->file)}}">{{ $kandidat->file }}</a></p>

                    <a href="{{ route('kandidat.edit', $kandidat->id) }}" class="btn btn-primary">Edit kandidat</a>

                    <div class="col-md-6 text-right pull-right">
                        {!! Form::open([
                            'method' => 'DELETE',
                            'route' => ['kandidat.destroy', $kandidat->id]
                        ]) !!}
                            {!! Form::submit('Delete this kandidat?', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection