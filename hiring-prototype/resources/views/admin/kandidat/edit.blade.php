@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">
                    Kandidat
                    <a href="{{ route('kandidat.show', $kandidat->id) }}" class="btn btn-primary pull-right">Back</a> 
                </div>

                <div class="card-body">
                    <h1>Editing "{{ $kandidat->nama_lengkap }}"</h1>

                    @if($errors->any())
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif

                    @if(Session::has('flash_message'))
                        <div class="alert alert-success">
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif

                    {!! Form::model($kandidat, [
                        'method' => 'PATCH',
                        'files' => 'true',
                        'route' => ['kandidat.update', $kandidat->id]
                    ]) !!}

                    <div class="form-group">
                        {!! Form::label('nama_lengkap', 'Nama Lengkap:', ['class' => 'control-label']) !!}
                        {!! Form::text('nama_lengkap', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('jenis_kelamin', 'Jenis Kelamin:', ['class' => 'control-label']) !!}
                        {!! Form::select('jenis_kelamin', array('pria' => 'Pria', 'wanita' => 'Wanita'), 'pria', ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('alamat', 'Alamat:', ['class' => 'control-label']) !!}
                        {!! Form::textarea('alamat', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('no_hp', 'No Handphone:', ['class' => 'control-label']) !!}
                        {!! Form::text('no_hp', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
                        {!! Form::email('email', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('lowongan_id', 'Lowongan:', ['class' => 'control-label']) !!}
                        {!! Form::select('lowongan_id', $lowongan, null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('status', 'Status:', ['class' => 'control-label']) !!}
                        {!! Form::select('status', array('review' => 'Review', 'diterima' => 'Diterima', 'ditolak' => 'Ditolak'), 'review', ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('file', 'File:', ['class' => 'control-label']) !!}
                        {!! Form::file('file', null, ['class' => 'form-control']) !!}
                    </div>

                    {!! Form::submit('Update kandidat', ['class' => 'btn btn-primary']) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection