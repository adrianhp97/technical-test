@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">
                    Lowongan Kerja
                    <a href="{{ route('home') }}" class="btn btn-primary pull-right">Back</a> 
                </div>

                <div class="card-body">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif

                    @if(Session::has('flash_message'))
                        <div class="alert alert-success">
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    
                    {!! Form::open([
                        'route' => 'kandidat.store'
                    ]) !!}

                    <div class="form-group">
                        {!! Form::hidden('nama_lengkap', Auth::user()->name, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('jenis_kelamin', 'Jenis Kelamin:', ['class' => 'control-label']) !!}
                        {!! Form::select('jenis_kelamin', array('pria' => 'Pria', 'wanita' => 'Wanita'), null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('alamat', 'Alamat:', ['class' => 'control-label']) !!}
                        {!! Form::textarea('alamat', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('no_hp', 'No Handphone:', ['class' => 'control-label']) !!}
                        {!! Form::text('no_hp', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::hidden('email', Auth::user()->email, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('lowongan_id', 'Lowongan:', ['class' => 'control-label']) !!}
                        {!! Form::select('lowongan_id', $lowongan, null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::hidden('status', 'review', ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('file', 'File:', ['class' => 'control-label']) !!}
                        {!! Form::file('file', null, ['class' => 'form-control']) !!}
                    </div>

                    {!! Form::submit('Create New Kandidat', ['class' => 'btn btn-primary']) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection