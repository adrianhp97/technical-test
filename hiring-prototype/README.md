# Backend Engineering Test
---

## Persoalan
PT Digdaya Olah Teknologi Indonesia sedang membuat prototype web application untuk
hiring manajemen yang mana HRD dapat mengelola data kandidat yang melamar.

---

## Cara Menjalankan Program
1. Lakukan cloning repositori dengan `git clone https://gitlab.com/adrianhp97/technical-test.git`
2. Masuk ke dalam folder hiring-prototype
3. Jalankan `composer install`
4. Buatlah database dengan nama `hiringdb` pada mysql
5. Buatlah file `.env` yang bisa dicopy dari file `.env.example` (Isikan informasi database yang diperlukan seperti DBNAME, USERNAME, dan PASSWORD)
6. Jalankan perintah `php artisan migrate`
7. Jalankan perintah `php artisan db:seed`
8. Jalankan perintah `php artisan serve`
9. Pada browser dapat dibuka `localhost:8000`
10. Untuk melakukan login, menggunakan akun dengan: Username=admin123 & Password=admin123