#include <iostream>
#include <vector>

using namespace std;

int beforeMax(vector<int> arrayOfNumbers) {
    int max, beforeMax;

    max = beforeMax = -1;

    for (int idx = 0; idx < arrayOfNumbers.size(); ++idx) {
        if (arrayOfNumbers[idx] >= max) {
            beforeMax = max == arrayOfNumbers[idx] ? beforeMax : max;
            max = arrayOfNumbers[idx];
        } else {
            if (arrayOfNumbers[idx] > beforeMax) {
                beforeMax = arrayOfNumbers[idx];
            }
        }
    }
    
    return beforeMax;
}

bool lengthValidation(int nArray) {
    if (nArray < 2) {
        cout << "Array length must larger or equal 2" << endl;
        return false;
    }
    return true;
}

int main() {
    int nArray;
    vector<int> arrayOfNumbers;

    do {
        cout << "Array Length: ";
        cin >> nArray;
    } while (!lengthValidation(nArray));

    cout << "Input Array Element: " << endl;
    for (int idx = 0; idx < nArray; ++idx) {
        int element;
        cin >> element;
        arrayOfNumbers.push_back(element);
    }
    
    cout << "Element before Max Element: " << beforeMax(arrayOfNumbers) << endl;
}