# Struktur Data & Logika
---

## Pertanyaan
Diberikan sebuah array S​ dengan sejumlah data integer di dalamnya. Carilah bilangan dengan
nilai terbesar kedua​ dari kumpulan integer tersebut dengan catatan bilangan dengan nilai yang
sama boleh muncul lebih dari satu kali.

**Contoh array:**

S = [2, 1, 6, 9, 9, 4, 3]

**Output​:**
6

**Penjelasan​:**
Dalam array S, bilangan 6 merupakan bilangan terbesar kedua setelah bilangan 9.
---

## Cara Menjalankan Program
1. Jalankan Makefile dengan perintah `make` pada terminal 
2. Jalankan `./main` pada terminal
3. Masukkan jumlah array
4. Masukkan isi dari array