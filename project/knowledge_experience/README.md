# Knowledge & Experience Questions
---

## Pertanyaan
1. Apa yang anda ketahui dengan struktur MVC? Jelaskan dan mengapa struktur tersebut
umum dipakai dan disarankan di web development
2. Apa yang anda ketahui dengan gitflow?
3. Apakah anda terbiasa menggunakan framework? Mengapa anda memilih framework
tersebut?
4. Bagaimana anda membuat design database yang baik?
5. Tools apa saja yang biasa anda gunakan dalam web development?
---

## Jawaban
1. MVC itu merupakan sebuah *patern* untuk arsitektur software. MVC baik digunakan karena membagi tiga bagian berdasarkan kegunaannya. M yaitu *model* yang berhubungan dengan struktur data. V yaitu *view* yang berhubungan dengan *user inteface*. C yaitu *controller* yang berhubungan dengan *logic*. Selain itu, MVC mudah untuk dikembangkan karena bersifat modular, mudah dikombinasikan, juga fleksible.
2. *Gitflow* merupakan metode *branching* dalam *versioning*. *Gitflow* dipakai untuk mengembangkan aplikasi dengan cara tertentu. Ada beberapa macam gitflow contohnya seperti features branching, developer branching dan lain lainnya.
3. Ya, saya terbiasa menggunakan framework. Framework memudahkan dan memberikan struktur dalam pengembangan aplikasi. Aplikasi yang dibuat menjadi lebih terlihat rapih, selain itu juga beberapa hal mendasar tidak perlu diurusi lagi karena sudah disediakan oleh framework.
4. Pembuatan desain database yang baik dimulai dengan menentukan ada berapa entitas yang perlu dijadikan tabel. Setelah itu, menentukan relasi dari setiap tabel. Isi dari tabel sendiri juga perlu dipertimbangkan tipe, penyimpanan, dllnya.
5. Framework, Package Manager, Web Browser, Debugger pada browser